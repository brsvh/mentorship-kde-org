---
title: KDE & Google Summer of Code 2024
date: 2024-05-02
images:
  - images/gsoc-banner.png
authors:
 - Carl Schwan
SPDX-License-Identifier: CC-BY-SA-4.0
---

![](/images/gsoc-banner.png)

KDE will mentor ten projects in [Google Summer of Code (GSoC)](https://summerofcode.withgoogle.com/) this year. GSoC
is a program in which contributors new to open-source spend between 175 and 350
hours working on an open source project.

## Projects

### [KDE Connect](https://kdeconnect.kde.org/)

ShellWen Chen will work on updating the SSHD library in the KDE Connect Android app
which will improve the application's security and stability. Albert Vaca
Cintora will mentor this project.

### [Labplot](https://apps.kde.org/labplot2/)

LabPlot is a Data Visualization and Analysis platform. This summer, [Kuntal
Bar](https://invent.kde.org/kuntalhere) will work on adding 3D plotting support
to cater to the evolving demands of scientific research. [Israel
Galadima](https://invent.kde.org/izzygala) will work on Python wrappers around
the LabPlot C++ API. Alexander Semke will mentor both projects.

### [Arianna](https://apps.kde.org/arianna/)

Arianna is KDE's Epub viewer, and [Ajay](https://invent.kde.org/intincrab) will
work on porting the Javascript frontend from epub.js to Foliate.js, the library
that powers Foliate. Carl Schwan will mentor this project.

### [Frameworks](https://develop.kde.org/products/frameworks/)

[Manuel Alcaraz](https://invent.kde.org/manuelal) will work on adding support
for Qt for Python to some of the KDE Frameworks, enabling the large Python
ecosystem to use them. Carl Schwan will also mentor this project.

### [Okular](https://okular.kde.org)

[Pratham Gandhi](https://invent.kde.org/pgandhipro) will work on improving
Okular's support for Javascript forms. This is particularly important because
Javascript-powered forms are used frequently in PDFs provided by local
governments, and while Okular already partially supports them, many functions
are not implemented. Albert Astals Cid will mentor this project.

### [Snaps](https://snapcraft.io/)

[Soumyadeep Ghosh](https://invent.kde.org/soumyadghosh) will work under
Scarlett's direction and integrate the Snap ecosystem closer with KDE. This
includes fixing the Discover integration with Snap and adding a Snap KCM to
change the permission from Plasma System Settings.

### [Krita](https://krita.org)

[Ken Lo](https://invent.kde.org/kenlo) will work under the supervision of Tiar
and  Emmet O'Neill on improving the pixel art workflow by adding the Pixel
Perfect option to smooth out pixel art curves.

### [KDE Games](https://apps.kde.org/games)

[João Gouveia](https://invent.kde.org/joaotgouveia) will implement the backend
for a variant of the Mancala game as well as a solver under the supervision of
Benson Muite and Harsh Kumar.

### [Kdenlive](https://kdenlive.org)

[Chengkun Chen](https://invent.kde.org/seri) will work on improving the support
for subtitles in Kdenlive. More specifically, he will add full support for the
Sub Station Alpha v4.00+ format which contains style information for
Kdenlive. Jean-Baptiste Mardelle will mentor this project.

Let's warmly welcome all the new contributors and wish them a good summer within KDE!
